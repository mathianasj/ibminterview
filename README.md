# Blog Library
Provides the following functionality

* submit/retrieve posts
* add labels to a post
* add comments to a post
* retrieve labels for a post
* retrieve comments for a post
* retrieve posts that contain a label
* retrieve posts that were created by a user

All data is stored in an in-memory map, no data is persisted permanently

## Add Post Example
Demonstrates how to create a new post

```java
AddPostCriteria addPostCriteria = new AddPostCriteria();
addPostCriteria.setPost("post body");
addPostCriteria.setUserId("12345");
PostServiceImpl.getInstance().addPost(addPostCriteria);
```

## Add Comment Example
Demonstrates how to add a comment to an existing post

```java
AddCommentCriteria addCommentCriteria = new AddCommentCriteria();
addCommentCriteria.setComment("comment body");
addCommentCriteria.setPostId("1");
addCommentCriteria.setUserId("12345");
CommentServiceImpl.getInstance().addComment(addCommentCriteria);
```

## Add Label Example
Demonstrate how to add a label to an existing post

```java
AddLabelCriteria addLabelCriteria = new AddLabelCriteria();
addLabelCriteria.setLabel("labelA");
addLabelCriteria.setPostId("1");
addLabelCriteria.setUserId("12345");
LabelServiceImpl.getInstance().addLabel(addLabelCriteria);
```
