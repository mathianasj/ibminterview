package com.customer360.blog;

import java.util.List;

import com.customer360.blog.dto.AddPostCriteria;
import com.customer360.blog.dto.PostDTO;
import com.customer360.blog.exception.FieldException;
import com.customer360.blog.exception.SaveException;

/**
 * Handles all the functionality related to posts
 * 
 * @author mathianasj
 *
 */
public interface PostService {
  /**
   * Add a new post
   * 
   * @param addPostCriteria The criteria of the post to add
   * @return The newly created post
   * @throws SaveException exception for errors relating to saving the criteria
   */
  public PostDTO addPost(AddPostCriteria addPostCriteria) throws SaveException;

  /**
   * Retrieve the post for the given id
   * 
   * @param postId The post id
   * @return The retrieved post
   */
  public PostDTO getPostById(String postId);

  /**
   * Retrieve a list of posts associated to the given label
   * 
   * @param label The label to filter posts
   * @return List of posts with associated label provided
   * @throws FieldException exception when trying to access fields of the post
   */
  public List<PostDTO> getPostsWithLabel(String label) throws FieldException;

  /**
   * Retrieve a list of posts for a given user
   * 
   * @param userId The user id to filter posts
   * @return List of posts created by user id provided
   * @throws FieldException exception when trying to access fields of the post
   */
  public List<PostDTO> getPostsWithUser(String userId) throws FieldException;

  /**
   * Retrieve a list of all posts in the system, if this was a fully developed application I would
   * have used filter criteria to limit results and enable paging abilities
   * 
   * @return The list of posts
   */
  public List<PostDTO> getAllPosts();
}
