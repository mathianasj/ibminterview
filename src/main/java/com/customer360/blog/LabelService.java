package com.customer360.blog;

import java.util.List;

import com.customer360.blog.dto.AddLabelCriteria;
import com.customer360.blog.dto.LabelDTO;
import com.customer360.blog.exception.FieldException;
import com.customer360.blog.exception.SaveException;

/**
 * Handles all the functionality related to labels
 * 
 * @author mathianasj
 *
 */
public interface LabelService {
  /**
   * Add a label
   * 
   * @param addLabelCriteria The criteria of the label to add
   * @return The newly created label
   * @throws SaveException exception for errors relating to saving the criteria
   */
  public LabelDTO addLabel(AddLabelCriteria addLabelCriteria) throws SaveException;

  /**
   * Get all labels with a label
   * 
   * @param label the label
   * @return list of labels with the provided label
   * @throws FieldException exception when trying to access fields of the label
   */
  public List<LabelDTO> getLabelsWithLabel(String label) throws FieldException;

  /**
   * Get all labels assigned to a post
   * 
   * @param postId post id to retrieve labels for
   * @return List of labels for a given post id
   * @throws FieldException exception when trying to access fields of the label
   */
  public List<LabelDTO> getLabelsWithPost(String postId) throws FieldException;
}
