package com.customer360.blog;

import java.util.List;

import com.customer360.blog.dao.LabelDAO;
import com.customer360.blog.dao.LabelDAOImpl;
import com.customer360.blog.dto.AddLabelCriteria;
import com.customer360.blog.dto.LabelDTO;
import com.customer360.blog.exception.FieldException;
import com.customer360.blog.exception.SaveException;

/**
 * Default implementation of the label service
 * 
 * @author mathianasjs
 *
 */
public class LabelServiceImpl implements LabelService {
  LabelDAO labelDAO;
  private static LabelServiceImpl instance = new LabelServiceImpl();

  private LabelServiceImpl() {
    this.labelDAO = LabelDAOImpl.getInstance();
  }

  public static LabelServiceImpl getInstance() {
    return instance;
  }

  @Override
  public LabelDTO addLabel(AddLabelCriteria addLabelCriteria) throws SaveException {
    LabelDTO objToSave = new LabelDTO();

    this.validateCriteria(addLabelCriteria);

    objToSave.setLabel(addLabelCriteria.getLabel());
    objToSave.setPostId(addLabelCriteria.getPostId());
    objToSave.setUserId(addLabelCriteria.getUserId());

    return labelDAO.save(objToSave);
  }

  private void validateCriteria(AddLabelCriteria addLabelCriteria) throws SaveException {
    if (addLabelCriteria == null) {
      throw new SaveException("label criteria is required");
    }

    this.validateLabel(addLabelCriteria);
    this.validatePostId(addLabelCriteria);
    this.validateUserId(addLabelCriteria);
    this.validateExistingLabel(addLabelCriteria);
  }

  private void validateLabel(AddLabelCriteria addLabelCriteria) throws SaveException {
    if (addLabelCriteria.getLabel() == null || addLabelCriteria.getLabel().isEmpty()) {
      throw new SaveException("label is required");
    }
  }

  private void validatePostId(AddLabelCriteria addLabelCriteria) throws SaveException {
    if (addLabelCriteria.getPostId() == null || addLabelCriteria.getPostId().isEmpty()) {
      throw new SaveException("post id is required");
    }
  }

  private void validateUserId(AddLabelCriteria addLabelCriteria) throws SaveException {
    if (addLabelCriteria.getUserId() == null || addLabelCriteria.getUserId().isEmpty()) {
      throw new SaveException("user id is required");
    }
  }

  /**
   * This could be done more efficiently with a database server
   */
  private void validateExistingLabel(AddLabelCriteria addLabelCriteria) throws SaveException {
    List<LabelDTO> existingLabels;
    try {
      existingLabels = this.getLabelsWithLabel(addLabelCriteria.getLabel());
    } catch (FieldException e) {
      throw new SaveException(e);
    }

    for (LabelDTO label : existingLabels) {
      if (label.getPostId().equals(addLabelCriteria.getPostId())) {
        throw new SaveException("label already exists for post");
      }
    }
  }

  @Override
  public List<LabelDTO> getLabelsWithLabel(String label) throws FieldException {
    return labelDAO.getByField("label", label);
  }

  @Override
  public List<LabelDTO> getLabelsWithPost(String postId) throws FieldException {
    return labelDAO.getByField("postId", postId);
  }
}
