package com.customer360.blog.dao;

import com.customer360.blog.dto.CommentDTO;

public interface CommentDAO extends Dao<CommentDTO> {

}
