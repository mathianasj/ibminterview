package com.customer360.blog.dao;

import com.customer360.blog.dto.CommentDTO;

/**
 * Default in memory map DAO for CommentDTOs
 * 
 * @author mathianasjs
 *
 */
public final class CommentDAOImpl extends AbstractDAO<CommentDTO> implements CommentDAO {
  private static CommentDAOImpl instance = new CommentDAOImpl();

  private CommentDAOImpl() {
    super(CommentDTO.class);
  }

  /**
   * Get a singleton instance of the CommentDAOImpl
   * 
   * @return singleton CommentDAOImpl instance
   */
  public static CommentDAOImpl getInstance() {
    return instance;
  }
}
