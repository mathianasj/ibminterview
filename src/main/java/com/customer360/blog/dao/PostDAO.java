package com.customer360.blog.dao;

import com.customer360.blog.dto.PostDTO;

public interface PostDAO extends Dao<PostDTO> {

}
