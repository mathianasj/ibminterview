package com.customer360.blog.dao;

import com.customer360.blog.dto.LabelDTO;

public interface LabelDAO extends Dao<LabelDTO> {

}
