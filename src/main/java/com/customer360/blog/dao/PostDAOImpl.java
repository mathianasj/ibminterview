package com.customer360.blog.dao;

import com.customer360.blog.dto.PostDTO;

/**
 * Default in memory map DAO for PostDTOs
 * 
 * @author mathianasjs
 *
 */
public final class PostDAOImpl extends AbstractDAO<PostDTO> implements PostDAO {
  private static PostDAOImpl instance = new PostDAOImpl();

  private PostDAOImpl() {
    super(PostDTO.class);
  }

  /**
   * Get a singleton instance of the PostDAOImpl
   * 
   * @return singleton PostDAOImpl instance
   */
  public static PostDAOImpl getInstance() {
    return instance;
  }
}
