package com.customer360.blog.dao;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;

import com.customer360.blog.dto.Id;
import com.customer360.blog.exception.FieldException;
import com.customer360.blog.exception.IdentifierException;
import com.customer360.blog.exception.SaveException;

/**
 * provides default implementation for in memory map of objects
 * 
 * @author mathianasjs
 *
 * @param <T>
 */
public abstract class AbstractDAO<T> implements Dao<T> {
  Map<String, T> objects = new HashMap<String, T>();
  Map<String, Map<Object, List<String>>> indexes = new HashMap<String, Map<Object, List<String>>>();
  private Integer sequenceId = 0;

  protected AbstractDAO(Class<T> clazz) {
    this.initIndexes(clazz);
  }

  private void initIndexes(Class<T> clazz) {
    PropertyDescriptor[] fields = PropertyUtils.getPropertyDescriptors(clazz);

    for (PropertyDescriptor field : fields) {
      String fieldName = field.getName();

      indexes.put(fieldName, new HashMap<Object, List<String>>());
    }
  }

  @Override
  public final T getObjectById(String id) {
    return objects.get(id);
  }

  protected String getObjectId(T object) throws IdentifierException {
    Field idField = this.getObjectIdField(object);
    Method readMethod;
    String objectId;

    try {
      readMethod = this.getObjectIdReadMethod(idField, object);
      objectId = (String) readMethod.invoke(object);
    } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
        | IntrospectionException e) {
      throw new IdentifierException("Could not retrieve unique id", e);
    }

    return objectId;
  }

  protected void setObjectId(T object, String objectId) throws IdentifierException {
    Field idField = this.getObjectIdField(object);
    Method writeMethod;

    try {
      writeMethod = this.getObjectIdWriteMethod(idField.getName(), object);
    } catch (IntrospectionException e) {
      throw new IdentifierException("Could not get unique id setter method", e);
    }

    try {
      writeMethod.invoke(object, objectId);
    } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
      throw new IdentifierException("Could not set unique id", e);
    }
  }

  private Method getObjectIdWriteMethod(String fieldName, T object) throws IntrospectionException {
    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(fieldName, object.getClass());
    return PropertyUtils.getWriteMethod(propertyDescriptor);
  }

  private Method getObjectIdReadMethod(Field field, T object) throws IntrospectionException {
    PropertyDescriptor propertyDescriptor =
        new PropertyDescriptor(field.getName(), object.getClass());
    return PropertyUtils.getReadMethod(propertyDescriptor);
  }

  private Field getObjectIdField(T object) throws IdentifierException {
    Field[] fields = object.getClass().getDeclaredFields();
    Field output = null;

    for (Field field : fields) {
      if (field.isAnnotationPresent(Id.class)) {
        output = field;
        break;
      }
    }

    if (output == null) {
      throw new IdentifierException("Could not find unique id field");
    }

    return output;
  }

  @Override
  public List<T> getByField(String fieldName, String fieldValue) throws FieldException {
    List<T> output = new ArrayList<>();
    Map<Object, List<String>> fieldEntries = indexes.get(fieldName);
    List<String> objectIds;

    if (fieldEntries == null) {
      throw new FieldException("Field does not exist");
    } else {
      objectIds = fieldEntries.get(fieldValue);
    }

    if (objectIds != null) {
      for (String objectId : objectIds) {
        output.add(objects.get(objectId));
      }
    }

    return output;
  }

  @Override
  public T save(T objToSave) throws SaveException {
    String objectId = this.getNextAutoId().toString();

    try {
      this.setObjectId(objToSave, objectId);
    } catch (IdentifierException e) {
      throw new SaveException("Could not set new id", e);
    }

    this.indexFields(objToSave);

    this.objects.put(objectId, objToSave);

    return objToSave;
  }

  private void indexFields(T object) throws SaveException {
    PropertyDescriptor[] fields = PropertyUtils.getPropertyDescriptors(object.getClass());
    String objectId;
    try {
      objectId = this.getObjectId(object);

      for (PropertyDescriptor field : fields) {
        String fieldName = field.getName();

        Object fieldValue = PropertyUtils.getProperty(object, fieldName);

        if (indexes.get(fieldName) == null) {
          indexes.put(fieldName, new HashMap<Object, List<String>>());
        }

        if (indexes.get(fieldName).get(fieldValue) == null) {
          indexes.get(fieldName).put(fieldValue, new ArrayList<String>());
        }

        // add object id to index if not already added
        if (!indexes.get(fieldName).get(fieldValue).contains(objectId)) {
          indexes.get(fieldName).get(fieldValue).add(objectId);
        }
      }
    } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException
        | IdentifierException e) {
      throw new SaveException("Could not index fields", e);
    }
  }

  private Integer getNextAutoId() {
    this.sequenceId++;

    return this.sequenceId;
  }

  @Override
  public List<T> getObjectsByIds(List<String> ids) {
    List<T> output = new ArrayList<>();

    for (String id : ids) {
      output.add(objects.get(id));
    }

    return output;
  }

  @Override
  public List<T> getAllObjects() {
    return new ArrayList<T>(objects.values());
  }
}
