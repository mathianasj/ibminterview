package com.customer360.blog.dao;

import com.customer360.blog.dto.LabelDTO;

/**
 * Default in memory map DAO for LabelDTOs
 * 
 * @author mathianasjs
 *
 */
public final class LabelDAOImpl extends AbstractDAO<LabelDTO> implements LabelDAO {
  private static LabelDAOImpl instance = new LabelDAOImpl();

  private LabelDAOImpl() {
    super(LabelDTO.class);
  }

  /**
   * Get a singleton instance of the LabelDAOImpl
   * 
   * @return singleton instance of LabelDAOImpl
   */
  public static LabelDAOImpl getInstance() {
    return instance;
  }
}
