package com.customer360.blog.dao;

import java.util.List;

import com.customer360.blog.exception.FieldException;
import com.customer360.blog.exception.SaveException;

public interface Dao<T> {
  /**
   * Get an object by the annotated unique identifier
   * 
   * @param id the unique identifier for the object
   * @return the object matching the provided unique identifier
   */
  public T getObjectById(String id);

  /**
   * Get objects where a given field name equals the given contents
   * 
   * @param fieldName the name of the field to lookup
   * @param fieldValue the contents of the field value to match
   * @return List of fields with field name equal to field value provided
   * @throws FieldException exception when trying to access fields of the object
   */
  public List<T> getByField(String fieldName, String fieldValue) throws FieldException;

  /**
   * Save the provided object
   * 
   * @param objToSave the object to save
   * @return the object saved with appropriate values as they were saved and assign any unique
   *         identifiers required
   * @throws SaveException any errors that occur while saving the object
   */
  public T save(T objToSave) throws SaveException;

  /**
   * Get saved objects by the provided list of unique identifiers
   * 
   * @param ids The list of unique identifiers to retrieve the corresponding objects
   * @return the objects that match the provided unique identifiers
   */
  public List<T> getObjectsByIds(List<String> ids);

  /**
   * Get a list of all saved objects
   * 
   * @return all saved objects
   */
  public List<T> getAllObjects();
}
