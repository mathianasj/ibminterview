package com.customer360.blog;

import java.util.ArrayList;
import java.util.List;

import com.customer360.blog.dao.PostDAO;
import com.customer360.blog.dao.PostDAOImpl;
import com.customer360.blog.dto.AddPostCriteria;
import com.customer360.blog.dto.LabelDTO;
import com.customer360.blog.dto.PostDTO;
import com.customer360.blog.exception.FieldException;
import com.customer360.blog.exception.SaveException;

/**
 * Default implementation of the post service
 * 
 * @author mathianasjs
 *
 */
public class PostServiceImpl implements PostService {
  LabelService labelService;
  PostDAO postDAO;
  private static PostServiceImpl instance = new PostServiceImpl();

  private PostServiceImpl() {
    this.labelService = LabelServiceImpl.getInstance();
    this.postDAO = PostDAOImpl.getInstance();
  }

  public static PostServiceImpl getInstance() {
    return instance;
  }

  @Override
  public PostDTO addPost(AddPostCriteria addPostCriteria) throws SaveException {
    PostDTO objToSave = new PostDTO();

    this.validateAddPostCriteria(addPostCriteria);

    objToSave.setPost(addPostCriteria.getPost());
    objToSave.setUserId(addPostCriteria.getUserId());

    return postDAO.save(objToSave);
  }

  private void validateAddPostCriteria(AddPostCriteria addPostCriteria) throws SaveException {
    if (addPostCriteria == null) {
      throw new SaveException("criteria is required");
    }

    this.validatePost(addPostCriteria);
    this.validateUserId(addPostCriteria);
  }

  private void validatePost(AddPostCriteria addPostCriteria) throws SaveException {
    if (addPostCriteria.getPost() == null || addPostCriteria.getPost().isEmpty()) {
      throw new SaveException("post is required");
    }
  }

  private void validateUserId(AddPostCriteria addPostCriteria) throws SaveException {
    if (addPostCriteria.getUserId() == null || addPostCriteria.getUserId().isEmpty()) {
      throw new SaveException("user id is required");
    }
  }

  @Override
  public PostDTO getPostById(String postId) {
    return postDAO.getObjectById(postId);
  }

  @Override
  public List<PostDTO> getPostsWithLabel(String label) throws FieldException {
    List<LabelDTO> labels = labelService.getLabelsWithLabel(label);
    List<String> postIds = new ArrayList<>();

    for (LabelDTO labelObj : labels) {
      postIds.add(labelObj.getPostId());
    }

    return postDAO.getObjectsByIds(postIds);
  }

  @Override
  public List<PostDTO> getPostsWithUser(String userId) throws FieldException {
    return postDAO.getByField("userId", userId);
  }

  @Override
  public List<PostDTO> getAllPosts() {
    return postDAO.getAllObjects();
  }

}
