package com.customer360.blog;

import java.util.List;

import com.customer360.blog.dao.CommentDAO;
import com.customer360.blog.dao.CommentDAOImpl;
import com.customer360.blog.dto.AddCommentCriteria;
import com.customer360.blog.dto.CommentDTO;
import com.customer360.blog.exception.FieldException;
import com.customer360.blog.exception.SaveException;

/**
 * Default implementation of the comment service
 * 
 * @author mathianasjs
 *
 */
public class CommentServiceImpl implements CommentService {
  private CommentDAO commentDAO;
  private static CommentServiceImpl instance = new CommentServiceImpl();

  private CommentServiceImpl() {
    this.commentDAO = CommentDAOImpl.getInstance();
  }

  public static CommentServiceImpl getInstance() {
    return instance;
  }

  @Override
  public List<CommentDTO> getCommentsForPost(String postId) throws FieldException {
    return commentDAO.getByField("postId", postId);
  }

  @Override
  public CommentDTO addComment(AddCommentCriteria addCommentCriteria) throws SaveException {
    CommentDTO objToSave = new CommentDTO();

    this.validateAddCommentCriteria(addCommentCriteria);

    objToSave.setComment(addCommentCriteria.getComment());
    objToSave.setPostId(addCommentCriteria.getPostId());
    objToSave.setUserId(addCommentCriteria.getUserId());

    return commentDAO.save(objToSave);
  }

  private void validateAddCommentCriteria(AddCommentCriteria addCommentCriteria)
      throws SaveException {
    if (addCommentCriteria == null) {
      throw new SaveException("criteria is required");
    }

    this.validateComment(addCommentCriteria);
    this.validatePostId(addCommentCriteria);
    this.validateUserId(addCommentCriteria);
  }

  private void validateComment(AddCommentCriteria addCommentCriteria) throws SaveException {
    if (addCommentCriteria.getComment() == null || addCommentCriteria.getComment().isEmpty()) {
      throw new SaveException("comment is required");
    }
  }

  private void validatePostId(AddCommentCriteria addCommentCriteria) throws SaveException {
    if (addCommentCriteria.getPostId() == null || addCommentCriteria.getPostId().isEmpty()) {
      throw new SaveException("post id is required");
    }
  }

  private void validateUserId(AddCommentCriteria addCommentCriteria) throws SaveException {
    if (addCommentCriteria.getUserId() == null || addCommentCriteria.getUserId().isEmpty()) {
      throw new SaveException("user id is required");
    }
  }
}
