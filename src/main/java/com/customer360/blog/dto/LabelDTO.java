package com.customer360.blog.dto;

/**
 * A label about a post
 * 
 * @author mathianasj
 *
 */
public class LabelDTO extends AbstractDTO {
  private static final long serialVersionUID = -7173097305747028652L;
  @Id
  private String labelId;
  private String label;
  private String postId;
  private String userId;

  /**
   * Get the label
   * 
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * Set the label
   * 
   * @param label the label
   */
  public void setLabel(String label) {
    this.label = label;
  }

  /**
   * Get the label id
   * 
   * @return unique label id
   */
  public String getLabelId() {
    return labelId;
  }

  /**
   * Set the label id
   * 
   * @param labelId unique label id
   */
  public void setLabelId(String labelId) {
    this.labelId = labelId;
  }

  public String getPostId() {
    return postId;
  }

  public void setPostId(String postId) {
    this.postId = postId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }
}
