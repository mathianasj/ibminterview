package com.customer360.blog.dto;

/**
 * A comment about a post
 * 
 * @author mathianasj
 *
 */
public class CommentDTO extends AbstractDTO {
  private static final long serialVersionUID = -4443759139099105294L;
  @Id
  private String commentId;
  private String userId;
  private String comment;
  private String postId;

  /**
   * Get the unique comment id
   * 
   * @return unique comment id
   */
  public String getCommentId() {
    return commentId;
  }

  /**
   * Set the unique comment id
   * 
   * @param commentId unique comment id
   */
  public void setCommentId(String commentId) {
    this.commentId = commentId;
  }

  /**
   * Get the user id who created the comment
   * 
   * @return the user id who created the comment
   */
  public String getUserId() {
    return userId;
  }

  /**
   * Set the user id who created the comment
   * 
   * @param userId the user id who created the comment
   */
  public void setUserId(String userId) {
    this.userId = userId;
  }

  /**
   * Get the comment contents
   * 
   * @return comment contents
   */
  public String getComment() {
    return comment;
  }

  /**
   * Set the comment contents
   * 
   * @param comment comment connents
   */
  public void setComment(String comment) {
    this.comment = comment;
  }

  /**
   * Get the post id the comment is about
   * 
   * @return the post id the comment is associated
   */
  public String getPostId() {
    return postId;
  }

  /**
   * Set the post id the comment is about
   * 
   * @param postId the post id the comment is associated
   */
  public void setPostId(String postId) {
    this.postId = postId;
  }
}
