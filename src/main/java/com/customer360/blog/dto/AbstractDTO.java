package com.customer360.blog.dto;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Provides base functionality for all dto's including reflection hashCode and equals
 * 
 * @author mathianasj
 *
 */
public abstract class AbstractDTO implements Serializable {
  private static final long serialVersionUID = -989982111784467090L;

  /**
   * Determine if the provided object is equal to this one using reflection
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    return EqualsBuilder.reflectionEquals(this, obj);
  }

  /**
   * Generate the hash code of this object using reflection
   */
  @Override
  public int hashCode() {
    return HashCodeBuilder.reflectionHashCode(this);
  }
}
