package com.customer360.blog.dto;

/**
 * A post for the blog
 * 
 * @author mathianasj
 *
 */
public class PostDTO extends AbstractDTO {
  private static final long serialVersionUID = 2100241751295817831L;
  @Id
  private String postId;
  private String post;
  private String userId;

  /**
   * Get the post id
   * 
   * @return unique post id
   */
  public String getPostId() {
    return postId;
  }

  /**
   * Set the post id
   * 
   * @param postId unique post id
   */
  public void setPostId(String postId) {
    this.postId = postId;
  }

  /**
   * Get the post contents
   * 
   * @return post contents
   */
  public String getPost() {
    return post;
  }

  /**
   * Set the post contents
   * 
   * @param post post contents
   */
  public void setPost(String post) {
    this.post = post;
  }

  /**
   * Set the user id who created the post
   * 
   * @return user id who created the post
   */
  public String getUserId() {
    return userId;
  }

  /**
   * Get the user id who created the post
   * 
   * @param userId user id who created the post
   */
  public void setUserId(String userId) {
    this.userId = userId;
  }
}
