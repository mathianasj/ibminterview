package com.customer360.blog.dto;

/**
 * Criteria required to add a new comment
 * 
 * @author mathianasj
 *
 */
public class AddCommentCriteria extends AbstractDTO {
  private static final long serialVersionUID = 8140696557449486507L;
  private String comment;
  private String postId;
  private String userId;

  /**
   * Get the user id this comment will be associated to
   * 
   * @return The user id to associate this comment
   */
  public String getUserId() {
    return userId;
  }

  /**
   * Set the user id this comment will be associated to
   * 
   * @param userId The user id to associate this comment
   */
  public void setUserId(String userId) {
    this.userId = userId;
  }

  /**
   * Get the comment string
   * 
   * @return the comment string
   */
  public String getComment() {
    return comment;
  }

  /**
   * Sets the comment string
   * 
   * @param comment comment body
   */
  public void setComment(String comment) {
    this.comment = comment;
  }

  /**
   * Get the post id associated to the comment
   * 
   * @return The post id associated to this comment
   */
  public String getPostId() {
    return postId;
  }

  /**
   * Sets the post id associated to the comment
   * 
   * @param postId the assoicated post id
   */
  public void setPostId(String postId) {
    this.postId = postId;
  }
}
