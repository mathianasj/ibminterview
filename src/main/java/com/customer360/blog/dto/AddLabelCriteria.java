package com.customer360.blog.dto;

/**
 * Criteria to add a label
 * 
 * @author mathianasj
 *
 */
public class AddLabelCriteria extends AbstractDTO {
  private static final long serialVersionUID = 7421228297996749553L;
  private String label;
  private String postId;
  private String userId;

  /**
   * Get the label
   * 
   * @return The label
   */
  public String getLabel() {
    return label;
  }

  /**
   * Set the label
   * 
   * @param label the label to set
   */
  public void setLabel(String label) {
    this.label = label;
  }

  /**
   * Get the post id to assoicate the label with
   * 
   * @return The post id to associate to the label
   */
  public String getPostId() {
    return postId;
  }

  /**
   * Set the post id to associate the label with
   * 
   * @param postId the post id to associate to the label
   */
  public void setPostId(String postId) {
    this.postId = postId;
  }

  /**
   * Get the user id who attached the label
   * 
   * @return The user id attaching the label
   */
  public String getUserId() {
    return userId;
  }

  /**
   * Set the user id who attached the label
   * 
   * @param userId The user id attaching the label
   */
  public void setUserId(String userId) {
    this.userId = userId;
  }
}
