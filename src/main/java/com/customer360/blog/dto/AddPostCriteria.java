package com.customer360.blog.dto;

/**
 * The criteria to add a post
 * 
 * @author mathianasj
 *
 */
public class AddPostCriteria extends AbstractDTO {
  private static final long serialVersionUID = -6070776094434986214L;
  private String post;
  private String userId;

  /**
   * Get the post string
   * 
   * @return The post string contents
   */
  public String getPost() {
    return post;
  }

  /**
   * Set the post string
   * 
   * @param post the post string contents
   */
  public void setPost(String post) {
    this.post = post;
  }

  /**
   * Get the user id that created the post
   * 
   * @return the user id who created the post
   */
  public String getUserId() {
    return userId;
  }

  /**
   * Set the user id that created the post
   * 
   * @param userId the user id who created the post
   */
  public void setUserId(String userId) {
    this.userId = userId;
  }
}
