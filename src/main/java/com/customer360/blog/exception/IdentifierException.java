package com.customer360.blog.exception;

/**
 * Errors related to accessing the unique identifier of an object
 * 
 * @author mathianasjs
 *
 */
public class IdentifierException extends Exception {
  private static final long serialVersionUID = 546870027782493060L;

  public IdentifierException() {
    super();
  }

  public IdentifierException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public IdentifierException(String message, Throwable cause) {
    super(message, cause);
  }

  public IdentifierException(String message) {
    super(message);
  }

  public IdentifierException(Throwable cause) {
    super(cause);
  }

}
