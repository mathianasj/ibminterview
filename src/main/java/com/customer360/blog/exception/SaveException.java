package com.customer360.blog.exception;

/**
 * Errors related to saving objects
 * 
 * @author mathianasj
 *
 */
public class SaveException extends Exception {
  private static final long serialVersionUID = 1354218420393178390L;

  public SaveException() {}

  public SaveException(String message) {
    super(message);
  }

  public SaveException(Throwable cause) {
    super(cause);
  }

  public SaveException(String message, Throwable cause) {
    super(message, cause);
  }

  public SaveException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

}
