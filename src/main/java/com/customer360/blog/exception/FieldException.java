package com.customer360.blog.exception;

public class FieldException extends Exception {

  /**
   * Exceptions related to accessing attributes of an object
   */
  private static final long serialVersionUID = 3736907897708985970L;

  public FieldException() {}

  public FieldException(String message) {
    super(message);
  }

  public FieldException(Throwable cause) {
    super(cause);
  }

  public FieldException(String message, Throwable cause) {
    super(message, cause);
  }

  public FieldException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

}
