package com.customer360.blog;

import java.util.List;

import com.customer360.blog.dto.AddCommentCriteria;
import com.customer360.blog.dto.CommentDTO;
import com.customer360.blog.exception.FieldException;
import com.customer360.blog.exception.SaveException;

/**
 * Handles all of the functionality related to comments
 * 
 * @author mathianasj
 *
 */
public interface CommentService {
  /**
   * Get the list of comments associated to a given post id
   * 
   * @param postId The associated post id for the comment
   * @return List of comments associated to the post id
   * @throws FieldException exception related to fields inside the comment dto
   */
  public List<CommentDTO> getCommentsForPost(String postId) throws FieldException;

  /**
   * Add a comment
   * 
   * @param addCommentCriteria The criteria to create the comment
   * @return Newly added comment
   * @throws SaveException exception when saving the object for the criteria
   */
  public CommentDTO addComment(AddCommentCriteria addCommentCriteria) throws SaveException;
}
