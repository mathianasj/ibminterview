package com.customer360.blog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;
import org.unitils.reflectionassert.ReflectionAssert;

import com.customer360.blog.dao.LabelDAO;
import com.customer360.blog.dao.LabelDAOImpl;
import com.customer360.blog.dto.AddLabelCriteria;
import com.customer360.blog.dto.LabelDTO;
import com.customer360.blog.dto.PostDTO;
import com.customer360.blog.exception.FieldException;
import com.customer360.blog.exception.SaveException;

public class LabelServiceImplItTest {
  LabelService labelService = LabelServiceImpl.getInstance();
  LabelDAO labelDAO = LabelDAOImpl.getInstance();

  @Before
  public void before() throws Exception {
    Whitebox
        .setInternalState(labelDAO, "indexes", new HashMap<String, Map<Object, List<String>>>());
    Whitebox.setInternalState(labelDAO, "objects", new HashMap<String, PostDTO>());
    Whitebox.setInternalState(labelDAO, "sequenceId", 0);

    Whitebox.invokeMethod(labelDAO, "initIndexes", LabelDTO.class);
  }

  @Test
  public void addLabel() throws SaveException {
    final AddLabelCriteria addLabelCriteria = new AddLabelCriteria();
    final LabelDTO expected = new LabelDTO();
    final Map<String, LabelDTO> expectedLabels = new HashMap<>();
    final Map<String, Map<Object, List<String>>> expectedIndexes = new HashMap<>();
    LabelDTO actual;
    Map<String, LabelDTO> labels;
    Map<String, Map<Object, List<String>>> indexes;

    addLabelCriteria.setLabel("label123");
    addLabelCriteria.setPostId("post1");
    addLabelCriteria.setUserId("user1");

    expected.setLabelId("1");
    expected.setLabel("label123");
    expected.setPostId("post1");
    expected.setUserId("user1");

    actual = labelService.addLabel(addLabelCriteria);

    ReflectionAssert.assertReflectionEquals(expected, actual);

    // get comment that was saved
    labels = Whitebox.getInternalState(labelDAO, "objects");
    indexes = Whitebox.getInternalState(labelDAO, "indexes");

    expectedLabels.put("1", expected);

    expectedIndexes.put("labelId", new HashMap<Object, List<String>>());
    expectedIndexes.put("label", new HashMap<Object, List<String>>());
    expectedIndexes.put("postId", new HashMap<Object, List<String>>());
    expectedIndexes.put("userId", new HashMap<Object, List<String>>());
    expectedIndexes.put("class", new HashMap<Object, List<String>>());

    expectedIndexes.get("labelId").put("1", Arrays.asList("1"));
    expectedIndexes.get("label").put("label123", Arrays.asList("1"));
    expectedIndexes.get("postId").put("post1", Arrays.asList("1"));
    expectedIndexes.get("userId").put("user1", Arrays.asList("1"));
    expectedIndexes.get("class").put(expected.getClass(), Arrays.asList("1"));

    ReflectionAssert.assertReflectionEquals(expectedLabels, labels);
    ReflectionAssert.assertReflectionEquals(expectedIndexes, indexes);
  }

  @Test
  public void getLabelsWithLabel() throws FieldException, SaveException {
    final AddLabelCriteria addLabelCriteria1 = new AddLabelCriteria();
    final AddLabelCriteria addLabelCriteria2 = new AddLabelCriteria();
    final AddLabelCriteria addLabelCriteria3 = new AddLabelCriteria();
    final List<LabelDTO> expected = new ArrayList<>();
    final LabelDTO expectedLabel1 = new LabelDTO();
    final LabelDTO expectedLabel2 = new LabelDTO();
    List<LabelDTO> actual;

    addLabelCriteria1.setLabel("label1");
    addLabelCriteria1.setPostId("post1");
    addLabelCriteria1.setUserId("user1");

    addLabelCriteria2.setLabel("label2");
    addLabelCriteria2.setPostId("post2");
    addLabelCriteria2.setUserId("user1");

    addLabelCriteria3.setLabel("label1");
    addLabelCriteria3.setPostId("post3");
    addLabelCriteria3.setUserId("user1");

    expectedLabel1.setLabel("label1");
    expectedLabel1.setLabelId("1");
    expectedLabel1.setPostId("post1");
    expectedLabel1.setUserId("user1");

    expectedLabel2.setLabel("label1");
    expectedLabel2.setLabelId("3");
    expectedLabel2.setPostId("post3");
    expectedLabel2.setUserId("user1");

    expected.add(expectedLabel1);
    expected.add(expectedLabel2);

    labelService.addLabel(addLabelCriteria1);
    labelService.addLabel(addLabelCriteria2);
    labelService.addLabel(addLabelCriteria3);

    actual = labelService.getLabelsWithLabel("label1");

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }

  @Test
  public void getLabelsWithPost() throws FieldException, SaveException {
    final AddLabelCriteria addLabelCriteria1 = new AddLabelCriteria();
    final AddLabelCriteria addLabelCriteria2 = new AddLabelCriteria();
    final AddLabelCriteria addLabelCriteria3 = new AddLabelCriteria();
    final List<LabelDTO> expected = new ArrayList<>();
    final LabelDTO expectedLabel1 = new LabelDTO();
    final LabelDTO expectedLabel2 = new LabelDTO();
    List<LabelDTO> actual;

    addLabelCriteria1.setLabel("label1");
    addLabelCriteria1.setPostId("post1");
    addLabelCriteria1.setUserId("user1");

    addLabelCriteria2.setLabel("label2");
    addLabelCriteria2.setPostId("post2");
    addLabelCriteria2.setUserId("user1");

    addLabelCriteria3.setLabel("label1");
    addLabelCriteria3.setPostId("post3");
    addLabelCriteria3.setUserId("user1");

    expectedLabel1.setLabel("label1");
    expectedLabel1.setLabelId("1");
    expectedLabel1.setPostId("post1");
    expectedLabel1.setUserId("user1");

    expectedLabel2.setLabel("label1");
    expectedLabel2.setLabelId("3");
    expectedLabel2.setPostId("post3");
    expectedLabel2.setUserId("user1");

    expected.add(expectedLabel1);
    expected.add(expectedLabel2);

    labelService.addLabel(addLabelCriteria1);
    labelService.addLabel(addLabelCriteria2);
    labelService.addLabel(addLabelCriteria3);


    actual = labelService.getLabelsWithLabel("label1");

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }
}
