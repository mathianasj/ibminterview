package com.customer360.blog;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.unitils.reflectionassert.ReflectionAssert;

import com.customer360.blog.dao.CommentDAO;
import com.customer360.blog.dto.AddCommentCriteria;
import com.customer360.blog.dto.CommentDTO;
import com.customer360.blog.exception.FieldException;
import com.customer360.blog.exception.SaveException;

@RunWith(PowerMockRunner.class)
public class CommentServiceImplTest {
  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Mock
  CommentDAO commentDAO;

  @InjectMocks
  CommentServiceImpl commentServiceImpl;

  @Test
  public void getCommentsForPost() throws FieldException {
    final CommentDTO comment1 = new CommentDTO();
    final CommentDTO comment2 = new CommentDTO();
    final List<CommentDTO> expected = Arrays.asList(comment1, comment2);
    final String postId = "post1234";
    List<CommentDTO> actual;

    Mockito.when(commentDAO.getByField("postId", postId)).thenReturn(expected);

    actual = commentServiceImpl.getCommentsForPost(postId);

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }

  @Test
  public void addComment() throws SaveException {
    final CommentDTO expected = new CommentDTO();
    final CommentDTO objToSave = new CommentDTO();
    final AddCommentCriteria addCommentCriteria = new AddCommentCriteria();
    final String comment = "comment";
    final String postId = "post1234";
    final String userId = "user1234";
    final String commentId = "comment1234";
    CommentDTO actual;

    addCommentCriteria.setComment(comment);
    addCommentCriteria.setPostId(postId);
    addCommentCriteria.setUserId(userId);

    objToSave.setComment(comment);
    objToSave.setPostId(postId);
    objToSave.setUserId(userId);

    expected.setComment(comment);
    expected.setCommentId(commentId);
    expected.setPostId(postId);
    expected.setUserId(userId);

    Mockito.when(commentDAO.save(objToSave)).thenReturn(expected);

    actual = commentServiceImpl.addComment(addCommentCriteria);

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }

  @Test
  public void addCommentValidateNullCriteria() throws SaveException {
    thrown.expect(SaveException.class);
    thrown.expectMessage("criteria is required");

    commentServiceImpl.addComment(null);
  }

  @Test
  public void addCommentValidateNullComment() throws SaveException {
    final AddCommentCriteria addCommentCriteria = new AddCommentCriteria();

    thrown.expect(SaveException.class);
    thrown.expectMessage("comment is required");

    commentServiceImpl.addComment(addCommentCriteria);
  }

  @Test
  public void addCommentValidateEmptyComment() throws SaveException {
    final AddCommentCriteria addCommentCriteria = new AddCommentCriteria();

    thrown.expect(SaveException.class);
    thrown.expectMessage("comment is required");

    addCommentCriteria.setComment("");

    commentServiceImpl.addComment(addCommentCriteria);
  }

  @Test
  public void addCommentValidateNullPostId() throws SaveException {
    final AddCommentCriteria addCommentCriteria = new AddCommentCriteria();

    thrown.expect(SaveException.class);
    thrown.expectMessage("post id is required");

    addCommentCriteria.setComment("a");

    commentServiceImpl.addComment(addCommentCriteria);
  }

  @Test
  public void addCommentValidateEmptyPostId() throws SaveException {
    final AddCommentCriteria addCommentCriteria = new AddCommentCriteria();

    thrown.expect(SaveException.class);
    thrown.expectMessage("post id is required");

    addCommentCriteria.setComment("a");
    addCommentCriteria.setPostId("");

    commentServiceImpl.addComment(addCommentCriteria);
  }

  @Test
  public void addCommentValidateNullUserId() throws SaveException {
    final AddCommentCriteria addCommentCriteria = new AddCommentCriteria();

    thrown.expect(SaveException.class);
    thrown.expectMessage("user id is required");

    addCommentCriteria.setComment("a");
    addCommentCriteria.setPostId("a");

    commentServiceImpl.addComment(addCommentCriteria);
  }

  @Test
  public void addCommentValidateEmptyUserId() throws SaveException {
    final AddCommentCriteria addCommentCriteria = new AddCommentCriteria();

    thrown.expect(SaveException.class);
    thrown.expectMessage("user id is required");

    addCommentCriteria.setComment("a");
    addCommentCriteria.setPostId("a");
    addCommentCriteria.setUserId("");

    commentServiceImpl.addComment(addCommentCriteria);
  }

  @Test
  public void getInstance() {
    Assert.assertTrue(CommentServiceImpl.getInstance() instanceof CommentServiceImpl);
  }

}
