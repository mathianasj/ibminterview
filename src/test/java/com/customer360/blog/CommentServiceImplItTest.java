package com.customer360.blog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;
import org.unitils.reflectionassert.ReflectionAssert;

import com.customer360.blog.dao.CommentDAO;
import com.customer360.blog.dao.CommentDAOImpl;
import com.customer360.blog.dto.AddCommentCriteria;
import com.customer360.blog.dto.CommentDTO;
import com.customer360.blog.dto.PostDTO;
import com.customer360.blog.exception.FieldException;
import com.customer360.blog.exception.SaveException;

public class CommentServiceImplItTest {
  CommentService commentService = CommentServiceImpl.getInstance();
  CommentDAO commentDAO = CommentDAOImpl.getInstance();

  @Before
  public void before() throws Exception {
    Whitebox.setInternalState(commentDAO, "indexes",
        new HashMap<String, Map<Object, List<String>>>());
    Whitebox.setInternalState(commentDAO, "objects", new HashMap<String, PostDTO>());
    Whitebox.setInternalState(commentDAO, "sequenceId", 0);

    Whitebox.invokeMethod(commentDAO, "initIndexes", CommentDTO.class);
  }

  @Test
  public void addComment() throws SaveException {
    final AddCommentCriteria addCommentCriteria = new AddCommentCriteria();
    final CommentDTO expected = new CommentDTO();
    final Map<String, CommentDTO> expectedComments = new HashMap<>();
    final Map<String, Map<Object, List<String>>> expectedIndexes = new HashMap<>();
    CommentDTO actual;
    Map<String, CommentDTO> comments;
    Map<String, Map<Object, List<String>>> indexes;

    addCommentCriteria.setComment("comment");
    addCommentCriteria.setPostId("postId");
    addCommentCriteria.setUserId("userId");

    expected.setComment("comment");
    expected.setCommentId("1");
    expected.setPostId("postId");
    expected.setUserId("userId");

    actual = commentService.addComment(addCommentCriteria);

    ReflectionAssert.assertReflectionEquals(expected, actual);

    // get comment that was saved
    comments = Whitebox.getInternalState(commentDAO, "objects");
    indexes = Whitebox.getInternalState(commentDAO, "indexes");

    expectedComments.put("1", expected);

    expectedIndexes.put("commentId", new HashMap<Object, List<String>>());
    expectedIndexes.put("comment", new HashMap<Object, List<String>>());
    expectedIndexes.put("postId", new HashMap<Object, List<String>>());
    expectedIndexes.put("class", new HashMap<Object, List<String>>());
    expectedIndexes.put("userId", new HashMap<Object, List<String>>());

    expectedIndexes.get("commentId").put("1", Arrays.asList("1"));
    expectedIndexes.get("comment").put("comment", Arrays.asList("1"));
    expectedIndexes.get("postId").put("postId", Arrays.asList("1"));
    expectedIndexes.get("class").put(expected.getClass(), Arrays.asList("1"));
    expectedIndexes.get("userId").put("userId", Arrays.asList("1"));


    ReflectionAssert.assertReflectionEquals(expectedComments, comments);
    ReflectionAssert.assertReflectionEquals(expectedIndexes, indexes);
  }

  @Test
  public void getCommentsForPost() throws FieldException, SaveException {
    final List<CommentDTO> expected = new ArrayList<>();
    final CommentDTO expectedComment1 = new CommentDTO();
    final CommentDTO comment2 = new CommentDTO();
    final CommentDTO expectedComment2 = new CommentDTO();
    final AddCommentCriteria addCommentCriteria1 = new AddCommentCriteria();
    final AddCommentCriteria addCommentCriteria2 = new AddCommentCriteria();
    final AddCommentCriteria addCommentCriteria3 = new AddCommentCriteria();
    List<CommentDTO> actual;

    expectedComment1.setComment("comment1");
    expectedComment1.setCommentId("1");
    expectedComment1.setPostId("post1");
    expectedComment1.setUserId("user1");

    comment2.setComment("comment2");
    comment2.setCommentId("2");
    comment2.setPostId("post2");
    comment2.setUserId("user1");

    expectedComment2.setComment("comment3");
    expectedComment2.setCommentId("3");
    expectedComment2.setPostId("post1");
    expectedComment2.setUserId("user1");

    expected.add(expectedComment1);
    expected.add(expectedComment2);

    addCommentCriteria1.setComment("comment1");
    addCommentCriteria1.setPostId("post1");
    addCommentCriteria1.setUserId("user1");

    addCommentCriteria2.setComment("comment2");
    addCommentCriteria2.setPostId("post2");
    addCommentCriteria2.setUserId("user1");

    addCommentCriteria3.setComment("comment3");
    addCommentCriteria3.setPostId("post1");
    addCommentCriteria3.setUserId("user1");

    commentService.addComment(addCommentCriteria1);
    commentService.addComment(addCommentCriteria2);
    commentService.addComment(addCommentCriteria3);

    actual = commentService.getCommentsForPost("post1");

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }

  @Test
  public void getCommentsForPostEmptyPosts() throws FieldException {
    final List<CommentDTO> expected = new ArrayList<>();
    final List<CommentDTO> actual = commentService.getCommentsForPost("aab");

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }
}
