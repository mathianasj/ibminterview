package com.customer360.blog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.unitils.reflectionassert.ReflectionAssert;

import com.customer360.blog.dao.LabelDAO;
import com.customer360.blog.dto.AddLabelCriteria;
import com.customer360.blog.dto.LabelDTO;
import com.customer360.blog.exception.FieldException;
import com.customer360.blog.exception.SaveException;

@RunWith(PowerMockRunner.class)
public class LabelServiceImplTest {
  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Mock
  LabelDAO labelDAO;

  @InjectMocks
  LabelServiceImpl labelService;

  @Test
  public void addLabel() throws SaveException {
    final LabelDTO expected = new LabelDTO();
    final LabelDTO objToSave = new LabelDTO();
    final AddLabelCriteria addLabelCriteria = new AddLabelCriteria();
    final String label = "labelHere";
    final String labelId = "1234";
    final String postId = "2345";
    final String userId = "3456";
    LabelDTO actual;

    expected.setLabel(label);
    expected.setLabelId(labelId);
    expected.setPostId(postId);
    expected.setUserId(userId);

    addLabelCriteria.setLabel(label);
    addLabelCriteria.setPostId(postId);
    addLabelCriteria.setUserId(userId);

    objToSave.setLabel(label);
    objToSave.setPostId(postId);
    objToSave.setUserId(userId);

    PowerMockito.when(labelDAO.save(objToSave)).thenReturn(expected);

    actual = labelService.addLabel(addLabelCriteria);

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }

  @Test
  public void validateNullCriteria() throws SaveException {
    thrown.expect(SaveException.class);
    thrown.expectMessage("label criteria is required");

    labelService.addLabel(null);
  }

  @Test
  public void validateNullLabel() throws SaveException {
    final AddLabelCriteria addLabelCriteria = new AddLabelCriteria();

    thrown.expect(SaveException.class);
    thrown.expectMessage("label is required");

    labelService.addLabel(addLabelCriteria);
  }

  @Test
  public void validateEmptyLabel() throws SaveException {
    final AddLabelCriteria addLabelCriteria = new AddLabelCriteria();

    addLabelCriteria.setLabel("");

    thrown.expect(SaveException.class);
    thrown.expectMessage("label is required");

    labelService.addLabel(addLabelCriteria);
  }

  @Test
  public void validateNullPostId() throws SaveException {
    final AddLabelCriteria addLabelCriteria = new AddLabelCriteria();

    addLabelCriteria.setLabel("a");

    thrown.expect(SaveException.class);
    thrown.expectMessage("post id is required");

    labelService.addLabel(addLabelCriteria);
  }

  @Test
  public void validateEmptyPostId() throws SaveException {
    final AddLabelCriteria addLabelCriteria = new AddLabelCriteria();

    addLabelCriteria.setLabel("a");
    addLabelCriteria.setPostId("");

    thrown.expect(SaveException.class);
    thrown.expectMessage("post id is required");

    labelService.addLabel(addLabelCriteria);
  }

  @Test
  public void validateNullUserId() throws SaveException {
    final AddLabelCriteria addLabelCriteria = new AddLabelCriteria();

    addLabelCriteria.setLabel("a");
    addLabelCriteria.setPostId("b");

    thrown.expect(SaveException.class);
    thrown.expectMessage("user id is required");

    labelService.addLabel(addLabelCriteria);
  }

  @Test
  public void validateEmptyUserId() throws SaveException {
    final AddLabelCriteria addLabelCriteria = new AddLabelCriteria();

    addLabelCriteria.setLabel("a");
    addLabelCriteria.setPostId("b");
    addLabelCriteria.setUserId("");

    thrown.expect(SaveException.class);
    thrown.expectMessage("user id is required");

    labelService.addLabel(addLabelCriteria);
  }

  @Test
  public void getLabelsWithLabel() throws FieldException {
    final String label = "labela";
    final LabelDTO label1 = new LabelDTO();
    final LabelDTO label2 = new LabelDTO();
    final List<LabelDTO> expected = Arrays.asList(new LabelDTO[] {label1, label2});
    List<LabelDTO> actual;

    label1.setLabel(label);
    label1.setLabelId("1234");
    label2.setLabel(label);
    label2.setLabelId("2345");

    Mockito.when(labelDAO.getByField("label", label)).thenReturn(expected);

    actual = labelService.getLabelsWithLabel(label);

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }

  @Test
  public void getInstance() {
    Assert.assertTrue(LabelServiceImpl.getInstance() instanceof LabelServiceImpl);
  }

  @Test
  public void getLabelsWithPost() throws FieldException {
    final String postId = "3456";
    final LabelDTO label1 = new LabelDTO();
    final LabelDTO label2 = new LabelDTO();
    final List<LabelDTO> expected = Arrays.asList(new LabelDTO[] {label1, label2});
    List<LabelDTO> actual;

    label1.setPostId(postId);
    label1.setLabelId("1234");
    label2.setLabelId("2345");
    label2.setPostId(postId);

    Mockito.when(labelDAO.getByField("postId", postId)).thenReturn(expected);

    actual = labelService.getLabelsWithPost(postId);

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }

  @Test
  public void ValidateExistingLableFieldException() throws Exception {
    final String label = "label";
    final AddLabelCriteria addLabelCriteria = new AddLabelCriteria();

    addLabelCriteria.setLabel(label);

    thrown.expect(SaveException.class);
    thrown.expectMessage("message");

    PowerMockito
        .when(labelService,
            PowerMockito.method(LabelServiceImpl.class, "getLabelsWithLabel", String.class))
        .withArguments(addLabelCriteria.getLabel()).thenThrow(new FieldException("message"));

    Whitebox.invokeMethod(labelService, "validateExistingLabel", addLabelCriteria);
  }

  @Test
  public void ValidateExistingLablelExists() throws Exception {
    final String label = "label";
    final AddLabelCriteria addLabelCriteria = new AddLabelCriteria();
    final List<LabelDTO> existingLabels = new ArrayList<>();
    final LabelDTO existingLabel1 = new LabelDTO();
    final String postId = "12345";

    addLabelCriteria.setLabel(label);
    addLabelCriteria.setPostId(postId);

    existingLabel1.setPostId(postId);
    existingLabels.add(existingLabel1);

    thrown.expect(SaveException.class);
    thrown.expectMessage("label already exists for post");

    PowerMockito
        .when(labelService,
            PowerMockito.method(LabelServiceImpl.class, "getLabelsWithLabel", String.class))
        .withArguments(addLabelCriteria.getLabel()).thenReturn(existingLabels);

    Whitebox.invokeMethod(labelService, "validateExistingLabel", addLabelCriteria);
  }
}
