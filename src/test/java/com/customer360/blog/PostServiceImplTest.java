package com.customer360.blog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.unitils.reflectionassert.ReflectionAssert;

import com.customer360.blog.dao.PostDAO;
import com.customer360.blog.dto.AddPostCriteria;
import com.customer360.blog.dto.LabelDTO;
import com.customer360.blog.dto.PostDTO;
import com.customer360.blog.exception.FieldException;
import com.customer360.blog.exception.SaveException;

@RunWith(PowerMockRunner.class)
public class PostServiceImplTest {
  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Mock
  PostDAO postDAO;

  @Mock
  LabelService labelService;

  @InjectMocks
  PostServiceImpl postService;

  @Test
  public void addPost() throws SaveException {
    final AddPostCriteria addPostCriteria = new AddPostCriteria();
    final PostDTO expected = new PostDTO();
    final PostDTO objToSave = new PostDTO();
    final String post = "post message";
    final String postId = "1234";
    final String userId = "user12345";
    PostDTO actual;

    expected.setPost(post);
    expected.setPostId(postId);

    objToSave.setPost(post);
    objToSave.setUserId(userId);

    addPostCriteria.setPost(post);
    addPostCriteria.setUserId(userId);

    Mockito.when(postDAO.save(objToSave)).thenReturn(expected);

    actual = postService.addPost(addPostCriteria);

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }

  @Test
  public void getPostById() {
    final PostDTO expected = new PostDTO();
    final String post = "post message";
    final String postId = "12345";
    PostDTO actual;

    expected.setPost(post);
    expected.setPostId(postId);

    Mockito.when(postDAO.getObjectById(postId)).thenReturn(expected);

    actual = postService.getPostById(postId);

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }

  @Test
  public void getPostsWithLabel() throws FieldException {
    final PostDTO post1 = new PostDTO();
    final PostDTO post2 = new PostDTO();
    final String postId1 = "123456";
    final String postId2 = "234567";
    final List<PostDTO> expected = new ArrayList<>();
    final LabelDTO label1 = new LabelDTO();
    final LabelDTO label2 = new LabelDTO();
    final List<LabelDTO> labels = Arrays.asList(label1, label2);
    final String label = "label";
    List<PostDTO> actual;

    post1.setPost("post Message 1");
    post1.setPostId(postId1);
    post2.setPost("second post message");
    post2.setPostId(postId2);

    label1.setPostId(postId1);
    label2.setPostId(postId2);

    expected.add(post1);
    expected.add(post2);

    Mockito.when(labelService.getLabelsWithLabel(label)).thenReturn(labels);
    Mockito.when(postDAO.getObjectsByIds(Arrays.asList(postId1, postId2))).thenReturn(expected);

    actual = postService.getPostsWithLabel(label);

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }

  @Test
  public void getPostsWithUser() throws FieldException {
    final PostDTO post1 = new PostDTO();
    final PostDTO post2 = new PostDTO();
    final List<PostDTO> expected = new ArrayList<>();
    final String userId = "user12345";
    List<PostDTO> actual;

    post1.setPost("post Message 1");
    post1.setPostId("123456");
    post2.setPost("second post message");
    post2.setPostId("234567");

    expected.add(post1);
    expected.add(post2);

    Mockito.when(postDAO.getByField("userId", userId)).thenReturn(expected);

    actual = postService.getPostsWithUser(userId);

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }

  @Test
  public void getInstance() {
    Assert.assertTrue(PostServiceImpl.getInstance() instanceof PostServiceImpl);
  }

  @Test
  public void addPostValidateNullCriteria() throws SaveException {
    thrown.expect(SaveException.class);
    thrown.expectMessage("criteria is required");

    postService.addPost(null);
  }

  @Test
  public void addCommentValidateNullPost() throws SaveException {
    final AddPostCriteria addPostCriteria = new AddPostCriteria();

    thrown.expect(SaveException.class);
    thrown.expectMessage("post is required");

    postService.addPost(addPostCriteria);
  }

  @Test
  public void addCommentValidateEmptyAddPostCriteria() throws SaveException {
    final AddPostCriteria addPostCriteria = new AddPostCriteria();

    thrown.expect(SaveException.class);
    thrown.expectMessage("post is required");

    addPostCriteria.setPost("");

    postService.addPost(addPostCriteria);
  }

  @Test
  public void addCommentValidateNullUserId() throws SaveException {
    final AddPostCriteria addPostCriteria = new AddPostCriteria();

    thrown.expect(SaveException.class);
    thrown.expectMessage("user id is required");

    addPostCriteria.setPost("a");

    postService.addPost(addPostCriteria);
  }

  @Test
  public void addCommentValidateEmptyUserId() throws SaveException {
    final AddPostCriteria addPostCriteria = new AddPostCriteria();

    thrown.expect(SaveException.class);
    thrown.expectMessage("user id is required");

    addPostCriteria.setPost("a");
    addPostCriteria.setUserId("");

    postService.addPost(addPostCriteria);
  }

  @Test
  public void getAllPosts() {
    final List<PostDTO> expected = new ArrayList<>();
    final PostDTO expected1 = new PostDTO();
    List<PostDTO> actual;

    expected1.setPost("post message");
    expected1.setPostId("1");
    expected1.setUserId("2");

    PowerMockito.when(postDAO.getAllObjects()).thenReturn(expected);

    actual = postService.getAllPosts();

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }
}
