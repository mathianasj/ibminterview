package com.customer360.blog.dao;

import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.unitils.reflectionassert.ReflectionAssert;

import com.customer360.blog.dto.Id;
import com.customer360.blog.exception.FieldException;
import com.customer360.blog.exception.IdentifierException;
import com.customer360.blog.exception.SaveException;

@RunWith(PowerMockRunner.class)
@PrepareForTest({AbstractDAO.class, AbstractDAOTest.TestClass.class})
public class AbstractDAOTest {
  @Rule
  public ExpectedException thrown = ExpectedException.none();

  TestClass testClass;
  TestExceptionsClass testExceptionsClass;

  @Before
  public void before() {
    this.testClass = PowerMockito.spy(new TestClass());
    this.testExceptionsClass = PowerMockito.spy(new TestExceptionsClass());
  }

  @Test
  public void getObjectById() {
    final TestObj expected = new TestObj();
    final Map<String, TestObj> objects = new HashMap<String, TestObj>();
    final String objectId = "22314";
    TestObj actual;

    expected.setTestObjId(objectId);
    expected.setMessage("a message here");
    objects.put(objectId, expected);

    Whitebox.setInternalState(testClass, "objects", objects);

    actual = testClass.getObjectById(objectId);

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }

  @Test
  public void save() throws SaveException {
    final TestObj expected = new TestObj();
    Map<String, Map<Object, List<String>>> expectedIndexes =
        new HashMap<String, Map<Object, List<String>>>();
    Map<String, Map<Object, List<String>>> actualIndexes;
    TestObj objToSave = new TestObj();
    TestObj actual;
    final String testObjId = "1";
    final String message = "a message here";

    objToSave.setMessage(message);

    expected.setTestObjId(testObjId);
    expected.setMessage(message);

    actual = testClass.save(objToSave);

    // validate auto increment
    ReflectionAssert.assertReflectionEquals(expected, actual);

    // setup expected indexes
    expectedIndexes.put("message", new HashMap<Object, List<String>>());

    expectedIndexes.get("message").put("a message here", new ArrayList<String>());
    expectedIndexes.get("message").put("class", new ArrayList<String>());
    expectedIndexes.get("message").put("testObjId", new ArrayList<String>());

    expectedIndexes.get("message").get("a message here").add("1");
    expectedIndexes.get("message").get("class").add("1");
    expectedIndexes.get("message").get("testObjId").add("1");

    // validate indexes
    actualIndexes = Whitebox.getInternalState(testClass, "indexes");
    ReflectionAssert.assertReflectionEquals(expected, actual);
  }

  @Test
  public void getByField() throws SaveException, FieldException {
    final TestObj testObj1 = new TestObj();
    final TestObj testObj2 = new TestObj();
    final TestObj testObj3 = new TestObj();
    List<TestObj> actual;
    final List<TestObj> expected = Arrays.asList(testObj1, testObj3);

    // setup test objects
    testObj1.setMessage("message 1");
    testObj2.setMessage("message 2");
    testObj3.setMessage("message 1");

    // save all
    testClass.save(testObj1);
    testClass.save(testObj2);
    testClass.save(testObj3);


    // get by message
    actual = testClass.getByField("message", "message 1");

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }

  @Test
  public void getByFieldNullField() throws FieldException {
    thrown.expect(FieldException.class);
    thrown.expectMessage("Field does not exist");

    testClass.getByField("fieldName", "abc");
  }

  @Test
  public void getByFieldNullFieldValue() throws FieldException {
    final List<TestObj> expected = new ArrayList<>();
    List<TestObj> actual;

    Map<String, Map<Object, List<String>>> indexes =
        new HashMap<String, Map<Object, List<String>>>();
    indexes.put("fieldName", new HashMap<Object, List<String>>());

    Whitebox.setInternalState(testClass, "indexes", indexes);

    actual = testClass.getByField("fieldName", "abc");

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }

  @Test
  public void getObjectsByIds() throws SaveException {
    final TestObj testObj1 = new TestObj();
    final TestObj testObj2 = new TestObj();
    final TestObj testObj3 = new TestObj();
    final String testObj1Id = "1";
    final String testObj3Id = "3";
    final List<TestObj> expected = Arrays.asList(testObj1, testObj3);
    List<TestObj> actual;

    testObj1.setMessage("message 1");
    testObj2.setMessage("message 2");
    testObj3.setMessage("message 3");

    // save all
    testClass.save(testObj1);
    testClass.save(testObj2);
    testClass.save(testObj3);

    actual = testClass.getObjectsByIds(Arrays.asList(testObj1Id, testObj3Id));

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }

  @Test
  public void getObjectIdException() throws Exception {
    TestObjExceptions testObj = new TestObjExceptions();

    thrown.expect(IdentifierException.class);
    thrown.expectMessage("Could not retrieve unique id");

    testExceptionsClass.getObjectId(testObj);
  }

  @Test
  public void setObjectIdIntrospectionException() throws Exception {
    thrown.expect(IdentifierException.class);
    thrown.expectMessage("Could not get unique id setter method");

    TestObj testObj = new TestObj();

    PowerMockito.when(testClass, "getObjectIdWriteMethod", "testObjId", testObj).thenThrow(
        new IntrospectionException("error"));

    testClass.setObjectId(testObj, "1234");
  }

  @Test
  public void setObjectIdInvokeException() throws Exception {
    TestObjExceptions testObj = new TestObjExceptions();

    thrown.expect(IdentifierException.class);
    thrown.expectMessage("Could not set unique id");

    testExceptionsClass.setObjectId(testObj, "1234");
  }

  @Test
  public void getObjectIdField() throws Exception {
    TestEmptyClassObj testObj = new TestEmptyClassObj();
    TestEmptyClass testEmptyClass = new TestEmptyClass();

    thrown.expect(IdentifierException.class);
    thrown.expectMessage("Could not find unique id field");

    Whitebox.invokeMethod(testEmptyClass, "getObjectIdField", testObj);
  }

  @Test
  public void getByFieldDoesNotExist() throws FieldException {
    thrown.expect(FieldException.class);
    thrown.expectMessage("Field does not exist");

    testClass.getByField("field", "value");
  }

  @Test
  public void saveCouldNotSetNewId() throws Exception {
    final TestObj testObj = new TestObj();

    PowerMockito.when(testClass, "setObjectId", testObj, "1").thenThrow(new IdentifierException());

    thrown.expect(SaveException.class);
    thrown.expectMessage("Could not set new id");

    testClass.save(testObj);
  }

  @Test
  public void indexFieldsTwice() throws Exception {
    final TestObj testObj = new TestObj();
    Map<String, Map<Object, List<String>>> expected = new HashMap<>();
    testObj.setMessage("message");
    testObj.setTestObjId("1234");

    Whitebox.invokeMethod(testClass, "indexFields", testObj);
    Whitebox.invokeMethod(testClass, "indexFields", testObj);

    Map<String, Map<Object, List<String>>> actual = Whitebox.getInternalState(testClass, "indexes");

    expected.put("message", new HashMap<Object, List<String>>());
    expected.put("class", new HashMap<Object, List<String>>());
    expected.put("testObjId", new HashMap<Object, List<String>>());

    expected.get("message").put("message", Arrays.asList("1234"));
    expected.get("class").put(testObj.getClass(), Arrays.asList("1234"));
    expected.get("testObjId").put("1234", Arrays.asList("1234"));

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }

  @Test
  public void indexFieldsException() throws Exception {
    final TestObj testObj = new TestObj();
    thrown.expect(SaveException.class);
    thrown.expectMessage("Could not index fields");

    PowerMockito.when(testClass.getObjectId(testObj)).thenThrow(new IdentifierException());

    Whitebox.invokeMethod(testClass, "indexFields", testObj);
  }

  @Test
  public void getAllObjectsNoResults() throws Exception {
    final List<TestObj> expected = new ArrayList<>();
    List<TestObj> actual;

    actual = testClass.getAllObjects();

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }

  @Test
  public void getAllObjects() throws Exception {
    final List<TestObj> expected = new ArrayList<>();
    final Map<String, TestObj> objects = new HashMap<>();
    final TestObj expectedObj1 = new TestObj();
    final TestObj expectedObj2 = new TestObj();
    List<TestObj> actual;

    expectedObj1.setTestObjId("1");
    expectedObj1.setMessage("message1");
    expectedObj2.setTestObjId("2");
    expectedObj2.setMessage("message2");

    objects.put(expectedObj1.getTestObjId(), expectedObj1);
    objects.put(expectedObj2.getTestObjId(), expectedObj2);

    expected.add(expectedObj1);
    expected.add(expectedObj2);

    Whitebox.setInternalState(testClass, "objects", objects);

    actual = testClass.getAllObjects();

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }

  @Test
  public void initIndexes() throws Exception {
    final Map<String, Map<Object, List<String>>> expected =
        new HashMap<String, Map<Object, List<String>>>();
    Map<String, Map<Object, List<String>>> actual;
    Whitebox.setInternalState(testClass, "indexes",
        new HashMap<String, Map<Object, List<String>>>());
    Whitebox.invokeMethod(testClass, "initIndexes", TestObj.class);

    actual = Whitebox.getInternalState(testClass, "indexes");

    expected.put("testObjId", new HashMap<Object, List<String>>());
    expected.put("class", new HashMap<Object, List<String>>());
    expected.put("message", new HashMap<Object, List<String>>());

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }

  public class TestEmptyClass extends AbstractDAO<TestEmptyClassObj> {
    public TestEmptyClass() {
      super(TestEmptyClassObj.class);
    }
  }

  public class TestEmptyClassObj {
  }

  public class TestExceptionsClass extends AbstractDAO<TestObjExceptions> {
    public TestExceptionsClass() {
      super(TestObjExceptions.class);
    }
  }

  public class TestObjExceptions {
    @Id
    private String testObjId;

    public String getTestObjId() throws IllegalAccessException {
      throw new IllegalAccessException();
    }

    public void setTestObjId(String testObjId) throws IllegalAccessException {
      throw new IllegalAccessException();
    }
  }

  public class TestClass extends AbstractDAO<TestObj> {
    public TestClass() {
      super(TestObj.class);
    }
  }

  public class TestObj {
    @Id
    private String testObjId;
    private String message;

    public String getTestObjId() {
      return testObjId;
    }

    public void setTestObjId(String testObjId) {
      this.testObjId = testObjId;
    }

    public String getMessage() {
      return message;
    }

    public void setMessage(String message) {
      this.message = message;
    }
  }
}
