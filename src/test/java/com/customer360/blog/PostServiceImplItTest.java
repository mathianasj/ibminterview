package com.customer360.blog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;
import org.unitils.reflectionassert.ReflectionAssert;

import com.customer360.blog.dao.LabelDAO;
import com.customer360.blog.dao.LabelDAOImpl;
import com.customer360.blog.dao.PostDAO;
import com.customer360.blog.dao.PostDAOImpl;
import com.customer360.blog.dto.AddLabelCriteria;
import com.customer360.blog.dto.AddPostCriteria;
import com.customer360.blog.dto.LabelDTO;
import com.customer360.blog.dto.PostDTO;
import com.customer360.blog.exception.FieldException;
import com.customer360.blog.exception.SaveException;

public class PostServiceImplItTest {
  PostDAO postDAO = PostDAOImpl.getInstance();
  PostService postService = PostServiceImpl.getInstance();
  LabelService labelService = LabelServiceImpl.getInstance();
  LabelDAO labelDAO = LabelDAOImpl.getInstance();

  @Before
  public void before() throws Exception {
    Whitebox.setInternalState(postDAO, "indexes", new HashMap<String, Map<Object, List<String>>>());
    Whitebox.setInternalState(postDAO, "objects", new HashMap<String, PostDTO>());
    Whitebox.setInternalState(postDAO, "sequenceId", 0);

    Whitebox.invokeMethod(postDAO, "initIndexes", PostDTO.class);

    Whitebox
        .setInternalState(labelDAO, "indexes", new HashMap<String, Map<Object, List<String>>>());
    Whitebox.setInternalState(labelDAO, "objects", new HashMap<String, PostDTO>());
    Whitebox.setInternalState(labelDAO, "sequenceId", 0);

    Whitebox.invokeMethod(labelDAO, "initIndexes", LabelDTO.class);
  }

  @Test
  public void addAndGetPost() throws SaveException {
    final PostDTO expected = new PostDTO();
    final AddPostCriteria addPostCriteria = new AddPostCriteria();
    PostDTO savedPost;
    PostDTO getPost;

    addPostCriteria.setPost("post message");
    addPostCriteria.setUserId("userId1");

    expected.setPost("post message");
    expected.setPostId("1");
    expected.setUserId("userId1");

    savedPost = postService.addPost(addPostCriteria);
    postService.addPost(addPostCriteria);
    getPost = postService.getPostById("1");

    ReflectionAssert.assertReflectionEquals(expected, savedPost);
    ReflectionAssert.assertReflectionEquals(expected, getPost);
  }

  @Test
  public void getPostsWithLabel() throws SaveException, FieldException {
    final AddPostCriteria addPostCriteria1 = new AddPostCriteria();
    final AddPostCriteria addPostCriteria2 = new AddPostCriteria();
    final AddPostCriteria addPostCriteria3 = new AddPostCriteria();
    final AddLabelCriteria addLabelCriteria1 = new AddLabelCriteria();
    final AddLabelCriteria addLabelCriteria2 = new AddLabelCriteria();
    final AddLabelCriteria addLabelCriteria3 = new AddLabelCriteria();
    final PostDTO expected1 = new PostDTO();
    final PostDTO expected2 = new PostDTO();
    final List<PostDTO> expected = new ArrayList<>();
    List<PostDTO> actual;

    addPostCriteria1.setPost("expected1");
    addPostCriteria1.setUserId("userId1");

    addPostCriteria2.setPost("post message2");
    addPostCriteria2.setUserId("userId1");

    addPostCriteria3.setPost("post message3");
    addPostCriteria3.setUserId("userId1");

    addLabelCriteria1.setLabel("labela");
    addLabelCriteria1.setPostId("1");
    addLabelCriteria1.setUserId("userId2");

    addLabelCriteria2.setLabel("labelb");
    addLabelCriteria2.setPostId("1");
    addLabelCriteria2.setUserId("userId1");

    addLabelCriteria3.setLabel("labela");
    addLabelCriteria3.setPostId("3");
    addLabelCriteria3.setUserId("userId2");

    expected1.setPost("expected1");
    expected1.setPostId("1");
    expected1.setUserId("userId1");

    expected2.setPost("post message3");
    expected2.setPostId("3");
    expected2.setUserId("userId1");

    expected.add(expected1);
    expected.add(expected2);

    postService.addPost(addPostCriteria1);
    postService.addPost(addPostCriteria2);
    postService.addPost(addPostCriteria3);

    labelService.addLabel(addLabelCriteria1);
    labelService.addLabel(addLabelCriteria2);
    labelService.addLabel(addLabelCriteria3);

    actual = postService.getPostsWithLabel("labela");

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }

  @Test
  public void getPostsWithUser() throws SaveException, FieldException {
    final AddPostCriteria addPostCriteria1 = new AddPostCriteria();
    final AddPostCriteria addPostCriteria2 = new AddPostCriteria();
    final AddPostCriteria addPostCriteria3 = new AddPostCriteria();
    final PostDTO expected1 = new PostDTO();
    final PostDTO expected2 = new PostDTO();
    final List<PostDTO> expected = new ArrayList<>();
    List<PostDTO> actual;

    addPostCriteria1.setPost("expected1");
    addPostCriteria1.setUserId("userId2");

    addPostCriteria2.setPost("post message2");
    addPostCriteria2.setUserId("userId1");

    addPostCriteria3.setPost("post message3");
    addPostCriteria3.setUserId("userId2");

    expected1.setPost("expected1");
    expected1.setPostId("1");
    expected1.setUserId("userId2");

    expected2.setPost("post message3");
    expected2.setPostId("3");
    expected2.setUserId("userId2");

    expected.add(expected1);
    expected.add(expected2);

    postService.addPost(addPostCriteria1);
    postService.addPost(addPostCriteria2);
    postService.addPost(addPostCriteria3);

    actual = postService.getPostsWithUser("userId2");

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }

  @Test
  public void getAllPosts() throws SaveException, FieldException {
    final AddPostCriteria addPostCriteria1 = new AddPostCriteria();
    final AddPostCriteria addPostCriteria2 = new AddPostCriteria();
    final AddPostCriteria addPostCriteria3 = new AddPostCriteria();
    final PostDTO expected1 = new PostDTO();
    final PostDTO expected2 = new PostDTO();
    final PostDTO expected3 = new PostDTO();
    final List<PostDTO> expected = new ArrayList<>();
    List<PostDTO> actual;

    addPostCriteria1.setPost("expected1");
    addPostCriteria1.setUserId("userId2");

    addPostCriteria2.setPost("post message2");
    addPostCriteria2.setUserId("userId1");

    addPostCriteria3.setPost("post message3");
    addPostCriteria3.setUserId("userId2");

    expected1.setPost("expected1");
    expected1.setPostId("1");
    expected1.setUserId("userId2");

    expected2.setPost("post message2");
    expected2.setPostId("2");
    expected2.setUserId("userId1");

    expected3.setPost("post message3");
    expected3.setPostId("3");
    expected3.setUserId("userId2");

    expected.add(expected1);
    expected.add(expected2);
    expected.add(expected3);

    postService.addPost(addPostCriteria1);
    postService.addPost(addPostCriteria2);
    postService.addPost(addPostCriteria3);

    actual = postService.getAllPosts();

    ReflectionAssert.assertReflectionEquals(expected, actual);
  }
}
